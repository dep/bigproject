#! /usr/bin/env perl

use strict;
use warnings;

sub gen_file_pair {
  my ($base_name, $unitno, $fileno, $config) = @_;

  my $file_count    = $config->{FilesPerUnit};
  my $external_refs = $config->{ExternalRefs};
  my $internal_refs = $config->{InternalRefs};
  my $struct_count  = $config->{StructCount};

  open my $hpp, '>', "$base_name.hpp" or die;
  open my $cpp, '>', "$base_name.cpp" or die;

  print $hpp "#ifndef UNIT_${unitno}_${fileno}\n";
  print $hpp "#define UNIT_${unitno}_${fileno}\n\n";

  print $cpp "#include \"file_$fileno.hpp\"\n\n";

  # Include some random files from an earlier unit.
  if ($unitno > 0) {
    my $parent_count = $unitno;

    for (my $i = 0; $i < $external_refs; ++$i) {
      my $inc_unit = int rand($parent_count);
      my $inc_file = int rand($file_count);

      print $hpp "#include \"../unit_$inc_unit/file_$inc_file.hpp\"\n";
    }

    for (my $i = 0; $i < $internal_refs; ++$i) {
      my $inc_unit = int rand($parent_count);
      my $inc_file = int rand($file_count);

      print $cpp "#include \"../unit_$inc_unit/file_$inc_file.hpp\"\n";
    }
  }

  # Dump some structs
  for (my $i = 0; $i < $struct_count; ++$i) {
    my $struct_name = "SomeStruct_${unitno}_${fileno}_$i";
    my $member_name = "SomeMember_${unitno}_${fileno}_$i";
    my $function_name = "SomeFunction_${unitno}_${fileno}_$i";
    print $hpp "struct $struct_name\{\n";
    print $hpp "  int $member_name;\n";
    print $hpp "};\n\n";

    print $hpp "int $function_name($struct_name* ptr);\n";

    print $cpp "int $function_name($struct_name* ptr) {\n";
    print $cpp "  return ptr->$member_name * $i;\n";
    print $cpp "}\n";
  }

  print $hpp "#endif\n";
  close $hpp;
  close $cpp;
}

sub make_big_project {
  my $config = shift || {};

  $config->{UnitCount}    ||= 10;
  $config->{FilesPerUnit} ||= 300;
  $config->{ExternalRefs} ||= 3;
  $config->{InternalRefs} ||= 10;
  $config->{StructCount}  ||= 5;

  my $unit_count    = $config->{UnitCount};
  my $file_count    = $config->{FilesPerUnit};
  my $output_dir    = $config->{OutputDir}    || 'big-project';

  unless (-d $output_dir) { mkdir $output_dir or die; }

  for (my $unit = 0; $unit < $unit_count; ++$unit) {
    my $unit_dir = "$output_dir/unit_$unit";
    unless (-d $unit_dir) { mkdir $unit_dir or die; }
    for (my $file = 0; $file < $file_count; ++$file) {
      gen_file_pair "$unit_dir/file_$file", $unit, $file, $config;
    }
  }

  # Generate main.cpp
  open my $main, '>', "$output_dir/main.cpp" or die;
  print $main <<MAIN
#include "unit_9/file_40.hpp"

int main(int argc, char** argv)
{
  struct SomeStruct_9_40_4 test;
  test.SomeMember_9_40_4 = 7;
  return SomeFunction_9_40_4(&test);
}
MAIN
  ;
  close $main;

  # Generate tundra.lua
  open my $tlua, '>', "$output_dir/tundra.lua" or die;
  print $tlua <<LUA
local common = {
  Env = {
    CPPPATH = { "." },
    MY_CC_COMMON = {
      { "/W4", "/WX", "/wd4127", "/wd4100", "/wd4324", "/wd4624"; Config = "*-msvc-*" },
      { "-Wall", "-Werror", "-Wno-unused"; Config = { "*-gcc-*", "*-clang-*", "*-crosswin32" } },
      { "-g"; Config = { "*-gcc-debug", "*-clang-debug", "*-gcc-production", "*-clang-production" } },
      { "-O2"; Config = { "*-gcc-production", "*-clang-production", "*-crosswin32-production" } },
      { "-O3"; Config = { "*-gcc-release", "*-clang-release", "*-crosswin32-release" } },
      { "/MTd"; Config = "*-msvc-debug" },
      { "/O2"; Config = "*-msvc-production" },
      { "/Ox"; Config = "*-msvc-release" },
    },
    CCOPTS = "\$(MY_CC_COMMON)",
    CXXOPTS = {
      "\$(MY_CC_COMMON)",
      { "-std=c++11", "-fno-exceptions"; Config = { "*-gcc-*", "*-clang-*" } },
      { "-stdlib=libc++"; Config = { "*-clang-*" } },
    },
    CPPDEFS = {
      { "_WIN32", "WIN32_LEAN_AND_MEAN", "NOMINMAX"; Config = "*-msvc-*"  },
      { "_CRT_SECURE_NO_WARNINGS"; Config = "*-msvc-*"  },
      { "_DEBUG"; Config = "*-*-debug"  },
      { "NDEBUG"; Config = "*-*-release"  },
      { "TD_STANDALONE"; Config = "*-*-*-standalone"  },
    },

    GENERATE_PDB = { "1"; Config = { "*-msvc-debug", "*-msvc-production" } },
  },
  -- Link with the C++ compiler on clang++/gcc to get std library.
  ReplaceEnv = {
    LD = { "\$(CXX)"; Config = { "*-gcc-*", "*-clang-*", } }
  },
}


local function genwincfg(name, arch, vcversion)
  return Config {
    Name = name,
    Inherit = common,
    Tools = {
      { "msvc-winsdk"; TargetArch = arch, VcVersion = vcversion }
    }
  }
end

Build {
  Units = function()
    require "tundra.syntax.glob";

    local units = {}

    for i = 0, 9 do
      local name = string.format("unit_%d", i)
      units[#units+1] = StaticLibrary {
        Name = name,
        Sources = Glob { Dir = name, Extensions = { ".cpp" } }
      }
    end

    local main = Program {
      Name = "big-project",
      Sources = "main.cpp",
      Depends = units,
    }

    Default(main)

  end,

  Configs = {
    Config { Name = "macosx-clang", Inherit = common, Tools = { "clang-osx" }, DefaultOnHost = "macosx" },
    Config { Name = "linux-gcc",    Inherit = common, Tools = { "gcc" }, DefaultOnHost = "linux" },
    Config { Name = "freebsd-gcc",  Inherit = common, Tools = { "gcc" }, DefaultOnHost = "freebsd" },
    Config { Name = "openbsd-gcc",  Inherit = common, Tools = { "gcc" }, DefaultOnHost = "openbsd" },
    Config { Name = "win64-msvc",  Inherit = common, Tools = { "msvc-vs2012", TargetArch = "x64" }, DefaultOnHost = "windows" },
  },
}
LUA
;
  close $tlua;
}

make_big_project;
